import { ConnectionOptions } from 'typeorm';
import { ConfigModule } from '@nestjs/config';
ConfigModule.forRoot();

const config: ConnectionOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true, // No usar en produccion
  migrations: ['dist/src/db/migrations/*.js'],
  cli: { migrationsDir: 'src/db/migrations' },
};

export = config;
