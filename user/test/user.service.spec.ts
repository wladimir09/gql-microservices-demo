import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { UserService } from '../src/user/user.service';

describe('UserService', () => {
  let service: UserService;

  type MockType<T> = {
    [P in keyof T]?: jest.Mock<any>;
  };

  const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(
    () => ({
      findOneOrFail: jest.fn((entity) => entity),
      // ...
    }),
  );

  let repositoryMock: MockType<Repository<User>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        // Provide your mock instead of the actual repository
        {
          provide: getRepositoryToken(User),
          useFactory: repositoryMockFactory,
        },
      ],
    }).compile();
    service = module.get<UserService>(UserService);
    repositoryMock = module.get(getRepositoryToken(User));
  });

  it('should find a user', async () => {
    const user = { name: 'Alni', id: 1 };
    // Now you can control the return value of your mock's methods
    repositoryMock.findOneOrFail.mockReturnValue(user);
    expect(service.findOne(user.id)).toEqual(user);
    // And make assertions on how often and with what params your mock's methods are called
    expect(repositoryMock.findOneOrFail).toHaveBeenCalledWith(user.id);
  });
});
