import { ObjectType, Field, Int, Directive } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@ObjectType()
@Directive('@key(fields: "id")')
@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  @Field(() => Int)
  id: number;

  @Column()
  @Field()
  body: string;

  @Column({ type: 'timestamptz' })
  @Field()
  publicDate: Date;

  @Field((type) => User)
  user?: User;

  @Column()
  @Field(() => Int)
  authorId: number;
}
