import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostResolver } from './post.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from './entities/post.entity';
import { UsersResolvers } from './user.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([Post])],
  providers: [PostResolver, UsersResolvers, PostService],
})
export class PostModule {}
