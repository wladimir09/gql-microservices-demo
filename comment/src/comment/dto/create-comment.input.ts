import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateCommentInput {
  @Field()
  body: string;

  @Field()
  authorId: number;

  @Field()
  postId: number;

  @Field()
  publicDate: Date;
}
