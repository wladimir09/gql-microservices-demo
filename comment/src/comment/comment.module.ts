import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentResolver } from './comment.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { UsersResolvers } from './user.resolver';
import { PostsResolvers } from './post.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([Comment])],
  providers: [CommentResolver, UsersResolvers, PostsResolvers, CommentService],
})
export class CommentModule {}
