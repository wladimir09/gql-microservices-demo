import { Directive, Field, Int, ObjectType } from '@nestjs/graphql';
import { Comment } from './comment.entity';

@ObjectType()
@Directive('@extends')
@Directive('@key(fields: "id")')
export class Post {
  @Field((type) => Int)
  @Directive('@external')
  id: number;

  @Field((type) => [Comment])
  comments?: Comment[];
}
