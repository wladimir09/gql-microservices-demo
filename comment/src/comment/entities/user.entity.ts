import { Directive, Field, ID, ObjectType } from '@nestjs/graphql';
import { Comment } from './comment.entity';

@ObjectType()
@Directive('@extends')
@Directive('@key(fields: "id")')
export class User {
  @Field((type) => ID)
  @Directive('@external')
  id: number;

  @Field((type) => [Comment])
  comments?: Comment[];
}
