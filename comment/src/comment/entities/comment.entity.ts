import { ObjectType, Field, Int, Directive } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Post } from './post.entity';
import { User } from './user.entity';

@ObjectType()
@ObjectType()
@Directive('@key(fields: "id")')
@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  @Field(() => Int)
  id: number;

  @Column()
  @Field()
  body: string;

  @Column({ type: 'timestamptz' })
  @Field()
  publicDate: Date;

  @Field((type) => User)
  user?: User;

  @Column()
  @Field(() => Int)
  authorId: number;

  @Field((type) => Post)
  post?: Post;

  @Column()
  @Field(() => Int)
  postId: number;
}
