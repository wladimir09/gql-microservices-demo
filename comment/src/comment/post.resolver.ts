import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { Comment } from './entities/comment.entity';
import { Post } from './entities/post.entity';
import { CommentService } from './comment.service';

@Resolver((of) => Post)
export class PostsResolvers {
  constructor(private readonly commentService: CommentService) {}

  @ResolveField((of) => [Comment])
  public comments(@Parent() post: Post): Promise<Comment[]> {
    return this.commentService.forPost(post.id);
  }
}
