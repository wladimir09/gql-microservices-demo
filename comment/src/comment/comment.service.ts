import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCommentInput } from './dto/create-comment.input';
import { UpdateCommentInput } from './dto/update-comment.input';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment) private commentRepository: Repository<Comment>,
  ) {}

  create(createCommentInput: CreateCommentInput): Promise<Comment> {
    const newComment = this.commentRepository.create(createCommentInput);
    return this.commentRepository.save(newComment);
  }

  findAll(): Promise<Comment[]> {
    return this.commentRepository.find();
  }

  findOne(id: number): Promise<Comment> {
    return this.commentRepository.findOneOrFail(id);
  }

  forAuthor(id: number): Promise<Comment[]> {
    return this.commentRepository.find({ authorId: id });
  }

  forPost(id: number): Promise<Comment[]> {
    return this.commentRepository.find({ postId: id });
  }

  update(id: number, updateCommentInput: UpdateCommentInput) {
    return `This action updates a #${id} comment`;
  }

  remove(id: number) {
    return `This action removes a #${id} comment`;
  }
}
