import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { Comment } from './entities/comment.entity';
import { User } from './entities/user.entity';
import { CommentService } from './comment.service';

@Resolver((of) => User)
export class UsersResolvers {
  constructor(private readonly commentService: CommentService) {}

  @ResolveField((of) => [Comment])
  public comments(@Parent() user: User): Promise<Comment[]> {
    return this.commentService.forAuthor(user.id);
  }
}
